--1. Return the customerName of the customers who are from the Philippines

select * from customers where country= "Philippines";

--2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"

select contactLastName, contactFirstname from customers where customerName= "La Rochelle Gifts";

--3. Return the product name and MSRP of the product named "The Titanic"

select productName, MSRP from products where productName= "The Titanic";

--4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"

select firstName, lastName from employees where email= "jfirrelli@classicmodelcars.com";

--5. Return the names of customers who have no registered state.

select customerName from customers where state= "NULL";

--6. Return the first name, last name, email of the employee whose last name is Patterson and first name is steve

select firstName, lastName, email from employees where lastName= "Patterson" and firstName="Steve";


--7. Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000.

select customerName, country, creditLimit from customers where country != "USA" and creditLimit >=3000;

--8. Return the customers numbers of orders whose comments contain the string "DHL"

select customerNumber  from orders where comments like "%DHL%";

--9. Return the product lines whose text description mentions the phrase "state of the art"

select productLine  from productLines where textDescription like "%state of the art%";

--10. Return the countries of customers without duplication

select distinct country from customers;

--11. Return the statuses of orders without duplication

select distinct status from orders;

--12. Return the customer names and countries of customers whose country is "USA", "France", or Canada

select customerName, country from customers where country="USA" and country="France" or country="France";


--13. Return the first name, last name, and office's city of employesss whose offices are in Tokyo

select firstName, lastName, city from offices where city="Tokyo";

--14. Return the customer names of customers who were served by the employee names "Leslie Thompson"

select employeeNumber from employees where lastName="Thompson" and firstName="Leslie";
 
 --employee number of Leslie Thompson is 1166

 select customerName from customers where salesRepEmployeeNumber="1166";


--15. Return the product names and customer name of products ordered by "Baane Mini Imports"

select customerNumber from customers orders where customerName="Baane Mini Imports";

--customerNumber is 121

select orderNumber from orders where customerNumber="121";

--returns order numbers: 10103, 10158, 10309, 10325

select productCode from orderdetails where orderNumber="10103";
select productCode from orderdetails where orderNumber="10158";
select productCode from orderdetails where orderNumber="10309";
select productCode from orderdetails where orderNumber="10325";

--16. Return the 'first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country.

select firstName, customerName, country from employees and customer;

--17. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000.

select productName, quantityInStock from products where productLine like "%planes%" and quantityInstock<1000;

--18. Return the customer's name with a phone number containing "+81"

select customerName from customers where phone like "%+81%";
